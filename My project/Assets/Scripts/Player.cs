using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{

    public Vector2 initialPos;
    public Vector2 releasePos;
    public Vector2 finalPos;

    public Vector2 arrowDir;

    private bool isPressing;


    public float boostForce = 2;
    public float shootForce;

    public float shootAngle;

    private bool canShoot = true;


    Vector2 direction;



    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        GameManager.Instance.OnEnterPlayerTurn += PlayerTurnEvents;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.gameState == GameManager.GameState.playerPhase && canShoot)
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (Input.GetMouseButtonDown(0))
            {
                initialPos = mousePosition;
                isPressing = true;
                shootInfo.ShowDisplay();
            }
            else if (Input.GetMouseButtonUp(0))
            {
                canShoot = false;

                isPressing = false;

                shootInfo.HideDisplay();
                ShootArrow(shootForce);
                return;
            }

            if (isPressing)
            {
                Vector2 strenght = initialPos - mousePosition;
                shootForce = strenght.magnitude * boostForce;
                shootForce = Mathf.Clamp(shootForce, 0, 20);

                if (shootForce <= 20)
                {
                    shootInfo.UpdateLine(initialPos, mousePosition);
                    shootInfo.UpdateTexts(bow.transform.eulerAngles.z, shootForce);
                }

                direction = mousePosition - initialPos;

                bow.transform.right = -direction;
            }

        }
    }

    public void PlayerTurnEvents() //Responsável pelos eventos ao entrar no turno do player
    {
        if (health <= 0)
        {
            DisplayUI.Instance.EnemyWins();
            GameManager.Instance.EndGame();
            return;
        }

        DisplayUI.Instance.PlayersTurn();
        Cameras.Instance.ChangeToPlayerCamera();
        canShoot = true;
    }

    public void ShootArrow(float force)
    {
        audioSource.PlayOneShot(shootClip);
        GetArrow().ShootArrowSelf(bow.transform.right, force);

        //GameObject go = Instantiate(arrow, shootPos.position, bow.rotation);
        //go.GetComponent<Arrow>().ShootArrowSelf(bow.transform.right, force);
    }

}
