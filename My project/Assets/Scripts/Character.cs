using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public float health;
    public Transform bow;
    public Transform shootPos;
    public GameObject arrow;
    private int numArrow = 5;
    public int arrowIndex = 0;
    public List<Arrow> arrowList = new List<Arrow>();
    public ShootInfoDisplay shootInfo;

    public AudioClip shootClip;
    public AudioSource audioSource;

    // Start is called before the first frame update
    public virtual void Start()
    {
        for (int i = 0; i < numArrow; i++)
        {
            GameObject go = Instantiate(arrow, new Vector2(0, 0), Quaternion.identity);
            Arrow tempArrow = go.GetComponent<Arrow>();
            tempArrow.gameObject.SetActive(false);
            arrowList.Add(tempArrow);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void TakeDamage(float dmg)
    {
        health -= dmg;
    }

    public virtual Arrow GetArrow()
    {
        arrowIndex++;

        if (arrowIndex < arrowList.Count - 1)
        {
            arrowList[arrowIndex].gameObject.SetActive(false);
            arrowList[arrowIndex].myCollider.enabled = true;
            arrowList[arrowIndex].rb.bodyType = RigidbodyType2D.Dynamic;
            arrowList[arrowIndex].gameObject.SetActive(true);
            arrowList[arrowIndex].transform.position = shootPos.position;
        }
        else
        {
            arrowIndex = 0;
            arrowList[arrowIndex].gameObject.SetActive(false);
            arrowList[arrowIndex].myCollider.enabled = true;
            arrowList[arrowIndex].rb.bodyType = RigidbodyType2D.Dynamic;
            arrowList[arrowIndex].gameObject.SetActive(true);
            arrowList[arrowIndex].transform.position = shootPos.position;
        }

        return arrowList[arrowIndex];
    }
}
