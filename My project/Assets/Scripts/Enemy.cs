using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Enemy : Character
{

    public Transform dragStart;
    public Transform player;


    public int attempts;
    public int maxAttempts = 5;
    Vector2 VoDisplay;

    // Start is called before the first frame update
    public override void Start()
    {

        base.Start();
        GameManager.Instance.OnEnterEnemyTurn += EnemyTurnEvents;

        transform.position = new Vector2(UnityEngine.Random.Range(15, 30), -2.1f);
    }

    // Update is called once per frame
    void Update()
    {
       
        shootInfo.UpdateTexts(180 - bow.eulerAngles.z, VoDisplay.x); //Usado para atualizar o texto de for�a de disparo

        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(ShootCoroutine());
        }

    }

    public void EnemyTurnEvents() //Respons�vel pelos eventos ao entrar no turno do inimigo
    {

        if (health <= 0)
        {
            DisplayUI.Instance.PlayerWins();
            GameManager.Instance.EndGame();
            
        }
        else
        {
            DisplayUI.Instance.EnemysTurn();
            Cameras.Instance.ChangeToEnemyCamera();
            StartCoroutine(ShootCoroutine());
        }
    }

    IEnumerator ShootCoroutine()
    {
        yield return new WaitForSeconds(2);

        shootInfo.ShowDisplay();

        Vector2 Vo = CalculateVelocity(player.position, bow.transform.position, 1);

        int rand = UnityEngine.Random.Range(0, 3);

        Debug.Log(rand);

        //Cria a randomicidade dos disparo do inimigo, a cada disparo a var�vel attempts � acrescentada em 1, at� que se iguala ao maxAttmpts que � quando o inimigo acerta a cabe�a do player
        if (rand == 0)
        {
            Vo = CalculateVelocity(new Vector2(player.position.x + (attempts - maxAttempts), player.position.y - (attempts - maxAttempts)), bow.transform.position, 1);
        }
        else
        {
            Vo = CalculateVelocity(new Vector2(player.position.x - (attempts - maxAttempts), player.position.y), bow.transform.position, 1);
        }

        Vo.x = Mathf.Clamp(Vo.x, 0, 20);

        VoDisplay = Vo;

        float rotateTime = 0;

        //Rotaciona gradativamente o arco para a rota��o desejada(Vo)

        while (rotateTime < 1)
        {
            bow.right = Vector2.Lerp(bow.right, new Vector2(-Vo.x, Vo.y), Time.deltaTime);
            rotateTime += Time.deltaTime / 1.2f;

            yield return null;
        }

        float pullTime = 0;
        Vector2 endPullPosition = dragStart.position;

        //Puxa a linha gradativamente at� atingir a for�a desejada
        while (pullTime < 1)
        {
            endPullPosition = Vector2.Lerp(endPullPosition, (Vector2)dragStart.position + new Vector2(Vo.x, -Vo.y) / 2, Time.deltaTime);

            shootInfo.UpdateLine(dragStart.position, endPullPosition);

            pullTime += Time.deltaTime / 1.2f;

            yield return null;
        }

        yield return new WaitForSeconds(1.5f);

        shootInfo.HideDisplay();

        audioSource.PlayOneShot(shootClip);

        //GameObject go = Instantiate(arrow, shootPos.position, bow.rotation);
        //go.GetComponent<Arrow>().rb.velocity = new Vector2(-Vo.x, Vo.y);
        GetArrow().rb.velocity = new Vector2(-Vo.x, Vo.y);

        attempts += 1;
    }

    public Vector2 CalculateVelocity(Vector2 target, Vector2 origin, float time)
    {
        Vector2 distance = target - origin;


        float Sy = distance.y;
        float Sx = distance.x;

        float Vx = Sx / time;
        float Vy = Sy / time + 0.5f * 9.81f * time;

        Vector2 result = distance.normalized;
        result *= Vx;
        result.y = Vy;

        return result;
    }


}
