using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Cameras : MonoBehaviour
{
    public static Cameras Instance;

    public CinemachineVirtualCamera virtualCamera;

    private void Awake()
    {
        Instance = this;
    }

    public GameObject playerCam;
    public GameObject enemyCam;
    public GameObject arrowCam;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeToPlayerCamera()
    {
        playerCam.SetActive(true);
        enemyCam.SetActive(false);
        arrowCam.SetActive(false);
    }
    public void ChangeToEnemyCamera()
    {
        playerCam.SetActive(false);
        enemyCam.SetActive(true);
        arrowCam.SetActive(false);
    }
    public void ChangeToArrowCamera(Transform tr)
    {
        virtualCamera.m_Follow = tr;

        playerCam.SetActive(false);
        enemyCam.SetActive(false);
        arrowCam.SetActive(true);
    }
}
