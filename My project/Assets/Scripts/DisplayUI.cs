using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class DisplayUI : MonoBehaviour
{
    public static DisplayUI Instance;

    public TextMeshProUGUI playerTurn;
    public TextMeshProUGUI enemyTurn;

    public CanvasGroup canvasGroup;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        PlayersTurn();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DisplayEndGame()
    {
        canvasGroup.gameObject.SetActive(true);
        canvasGroup.DOFade(0, 1f).OnComplete(() =>
        {
            canvasGroup.DOFade(1, 0.3f);
        });
    }

    public void EnemyWins()
    {
        GameManager.Instance.PlayLooseMusic();

        playerTurn.DOFade(0, 0f);

        enemyTurn.DOFade(1, 0.3f);
        enemyTurn.text = "Enemy Won!";
    }

    public void PlayerWins()
    {
        GameManager.Instance.PlayWinMusic();

        enemyTurn.DOFade(0, 0f);

        playerTurn.DOFade(1, 0.3f);
        playerTurn.text = "Player Won!";
    }

    public void PlayersTurn()
    {
        playerTurn.DOFade(1, 0.3f).OnComplete(() => playerTurn.DOFade(1, 1f).OnComplete(() =>
        {
            playerTurn.DOFade(0, 0.2f);
        }));
    }

    public void EnemysTurn()
    {
        enemyTurn.DOFade(1, 0.3f).OnComplete(() => enemyTurn.DOFade(1, 1f).OnComplete(() =>
        {
            enemyTurn.DOFade(0, 0.2f);
        }));
    }
}
