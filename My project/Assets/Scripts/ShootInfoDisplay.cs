using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShootInfoDisplay : MonoBehaviour
{

    public LineRenderer line;

    public TextMeshProUGUI angleText;
    public TextMeshProUGUI forceText;

    public CanvasGroup canvasGroup;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateTexts(float angle, float force)
    {
        angleText.transform.position = (Vector2)line.GetPosition(0);
        angleText.text = angle.ToString("0.00") + "�";

        forceText.transform.position = (Vector2)line.GetPosition(1);
        forceText.text = force.ToString("0.00");
    }

    public void HideDisplay()
    {
        canvasGroup.alpha = 0;
        line.gameObject.SetActive(false);
        UpdateLine(new Vector2(0, 1000), new Vector2(0, 1000));
    }

    public void ShowDisplay()
    {
        canvasGroup.alpha = 1;
        line.gameObject.SetActive(true);
    }

    public void UpdateLine(Vector2 point1Position, Vector2 point2Position)
    {
        line.SetPosition(0, point1Position);
        line.SetPosition(1, point2Position);
    }
}
