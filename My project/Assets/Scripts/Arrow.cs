using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    public Rigidbody2D rb;
    public float myAngle;
    public Collider2D myCollider;

    public ParticleSystem blood;

    public AudioClip hitGround, hitCharacter;
    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnEnable()
    {
        Cameras.Instance.ChangeToArrowCamera(transform);
    }

    // Update is called once per frame
    void Update()
    {
        if (rb.bodyType == RigidbodyType2D.Static)
            return;

        myAngle = Mathf.Atan2(rb.velocity.y, rb.velocity.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(myAngle, Vector3.forward);
    }

    public void ShootArrowSelf(Vector2 dir, float force)
    {
        Debug.Log("ShootArrow");
        rb.AddForce(dir * force, ForceMode2D.Impulse);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy") || collision.CompareTag("Player"))
        {
            Debug.Log("Hit: " + collision.name);

            if (rb.bodyType == RigidbodyType2D.Static)
                return;

            myCollider.enabled = false;
            rb.velocity = Vector2.zero;
            rb.bodyType = RigidbodyType2D.Static;

            audioSource.PlayOneShot(hitCharacter);
            blood.Play();

            Head head = collision.GetComponent<Head>();

            Character character = collision.GetComponent<Character>();

            if (head)
            {
                head.Kill();
                //GameManager.Instance.SwitchTurn();
            }
            else if (character != null)
            {
                character.TakeDamage(30);
            }

            GameManager.Instance.SwitchTurn();

        }
        else if (collision.CompareTag("Ground"))
        {
            if (rb.bodyType == RigidbodyType2D.Static)
                return;

            audioSource.PlayOneShot(hitGround);
            myCollider.enabled = false;
            rb.velocity = Vector2.zero;
            rb.bodyType = RigidbodyType2D.Static;
            GameManager.Instance.SwitchTurn();
        }


    }
}
