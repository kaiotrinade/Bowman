using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using System.Threading.Tasks;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;


    public Action OnEnterEnemyTurn;
    public Action OnEnterPlayerTurn;

    public enum GameState { playerPhase, enemyPhase, endGame };
    public GameState gameState;

    public AudioClip bgMusic, winMusic, looseMusic, switchTurnClip;
    public AudioSource audioSource;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        audioSource.loop = true;
        audioSource.clip = bgMusic;
        audioSource.Play();
    }

    public async void SwitchTurn()
    {
        //gameState = gameState == GameState.playerPhase ? GameState.enemyPhase : GameState.playerPhase;

        await Task.Delay(1000);

        audioSource.PlayOneShot(switchTurnClip);

        if (gameState == GameState.endGame)
        {

        }
        else if (gameState == GameState.playerPhase)
        {
            gameState = GameState.enemyPhase;
            OnEnterEnemyTurn?.Invoke();
        }
        else if (gameState == GameState.enemyPhase)
        {
            gameState = GameState.playerPhase;
            OnEnterPlayerTurn?.Invoke();

        }
    }

    public void PlayWinMusic()
    {
        audioSource.Stop();
        audioSource.PlayOneShot(winMusic);
    }

    public void PlayLooseMusic()
    {
        audioSource.Stop();
        audioSource.PlayOneShot(looseMusic);
    }

    public void EndGame()
    {
        gameState = GameState.endGame;

        DisplayUI.Instance.DisplayEndGame();
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene(0);
    }
}
